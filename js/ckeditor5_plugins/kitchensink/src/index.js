
import { Plugin } from 'ckeditor5/src/core';
import { ButtonView } from 'ckeditor5/src/ui';
import icon from '../icons/kitchensink.svg';

class KitchenSink extends Plugin {
  init() {
    const editor = this.editor;

    editor.ui.componentFactory.add('KitchenSink', (locale) => {
      const view = new ButtonView(locale);
      const toolbar = editor.ui.view.toolbar.element;
      const startCollapsed = window.localStorage.getItem('panopoly_wysiwyg_kitchensink') !== 'false';

      view.set({
        label: 'Show/hide toolbars',
        icon: icon,
        tooltip: true,
        isToggleable: true,
        isOn: startCollapsed,
      });

      function toggleKitchenSink(isCollapsed) {
        let isFirstRow = true;

        toolbar.querySelectorAll('.ck-toolbar__items > *').forEach((x) => {
          if (!isFirstRow) {
            x.style.display = isCollapsed ? 'none' : '';
          }
          if (x.classList.contains('ck-toolbar__line-break')) {
            isFirstRow = false;
          }
        });
      }
      window.setTimeout(() => {
        toggleKitchenSink(startCollapsed);
      }, 0);

      view.on('execute', () => {
        view.set('isOn', !view.isOn);
        toggleKitchenSink(view.isOn);
        window.localStorage.setItem('panopoly_wysiwyg_kitchensink', view.isOn);
      });

      return view;
    });
  }

  static get pluginName() {
    return 'KitchenSink';
  }
}

export default {
  KitchenSink,
}
